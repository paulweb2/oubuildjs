self.importScripts('data/games.js');

// Files to cache
const cacheName = 'oubuild-v2';
const appShellFiles = [
  'index.html',
  'app.js',
  'style.css',
  'fonts/graduate.eot',
  'fonts/graduate.ttf',
  'fonts/graduate.woff',
  'icons/icon-16.png',
  'icons/icon-32.png',
  'icons/icon-48.png',
  'icons/icon-128.png',
  'icons/icon-256.png',
  'img/copyCursor.png',
  'img/copyTool.png',
  'img/crosshairCursor.gif',
  'img/cutCursor.png',
  'img/cutTool.png',
  'img/fullScreenOff.png',
  'img/fullScreenOn.png',
  'img/greenFlagOff.png',
  'img/greenFlagOn.png',
  'img/growCursor.png',
  'img/growTool.png',
  'img/helpCursor.png',
  'img/helpTool.png',
  'img/oulogo.png',
  'img/shrinkCursor.png',
  'img/shrinkTool.png',
  'img/stopOff.png',
  'img/stopOn.png',
  'img/tm111_p01_03_owl_character_owl_a.png',
  'img/tm111_p01_03_owl_character_owl_b.png',
  'img/tm111_p03_12_owl_01.png',
  'img/cameraSmallOff.png',
  'img/cameraSmallOn.png',
  'img/importSmallOff.png',
  'img/importSmallOn.png',
  'img/landscapeSmallOff.png',
  'img/landscapeSmallOn.png',
  'img/paintbrushSmallOff.png',
  'img/paintbrushSmallOn.png',
  'img/cameraOff.png',
  'img/cameraOn.png',
  'img/importOff.png',
  'img/importOn.png',
  'img/landscapeOff.png',
  'img/landscapeOn.png',
  'img/libraryOff.png',
  'img/libraryOn.png',
  'img/paintbrushOff.png',
  'img/paintbrushOn.png',
];
const gamesImages = [];
for (let i = 0; i < games.length; i++) {
  gamesImages.push(`data/img/${games[i].slug}.jpg`);
}
const contentToCache = appShellFiles.concat(gamesImages);

// Installing Service Worker
self.addEventListener('install', (e) => {
  console.log('[Service Worker] Install');
  e.waitUntil((async () => {
    const cache = await caches.open(cacheName);
    console.log('[Service Worker] Caching all: app shell and content');
    await cache.addAll(contentToCache);
  })());
});

// Fetching content using Service Worker
self.addEventListener('fetch', (e) => {
  e.respondWith((async () => {
    const r = await caches.match(e.request);
    console.log(`[Service Worker] Fetching resource: ${e.request.url}`);
    if (r) return r;
    const response = await fetch(e.request);
    const cache = await caches.open(cacheName);
    console.log(`[Service Worker] Caching new resource: ${e.request.url}`);
    cache.put(e.request, response.clone());
    return response;
  })());
});
