let drag_selected = null;

// Registering Service Worker
if ('serviceWorker' in navigator) {
  navigator.serviceWorker.register('sw.js');
}

/**
 * Add functionality to the buttons in the toolbar
 */
// Declare function now so it can be used in removeEventListner when needed
const remove_toolbar_styles = function(evt) {
    evt.target.style.backgroundColor = null;
};
document.getElementById('topbar').querySelectorAll('.toolbar').forEach(function(item) {
    item.addEventListener('mouseover', function(evt) {
        evt.target.style.backgroundColor = '#179FD7';
    });
    item.addEventListener('mouseout', remove_toolbar_styles, false);
// Prevent new cursor from being removed
    item.addEventListener('click', function(evt) {
        const cursor_name = evt.target.dataset.cursor;
        evt.target.removeEventListener('mouseout', remove_toolbar_styles, false);
        evt.target.style.backgroundColor = '#179FD7';
        document.getElementById('topbar').style.cursor = `url(img/${cursor_name}.png), auto`;
        document.getElementById('main').style.cursor = `url(img/${cursor_name}.png), auto`;
    });
    item.addEventListener('blur', function(evt) {
        evt.target.addEventListener('mouseout', remove_toolbar_styles, false);
        evt.target.style.backgroundColor = null;
        document.getElementById('topbar').style.cursor = null;
        document.getElementById('main').style.cursor = null;
    })
});

/**
 * Add functionality to Stage
 */
document.getElementById('start_button').addEventListener('mouseover', function(evt) {
    evt.target.src = 'img/greenFlagOn.png';
});
document.getElementById('start_button').addEventListener('mouseout', function(evt) {
    evt.target.src = 'img/greenFlagOff.png';
});
let drag_x;
let drag_y;
document.getElementById('stage_main').addEventListener('mousemove', function(evt) {
console.log(evt.x, evt.y);
    let coord_reading_x = (evt.x - 5) - (evt.currentTarget.clientWidth / 2);
// Use offsetHeight to take in the 1px border-top that's listening
    let coord_reading_y = (evt.currentTarget.offsetHeight / 2) - (evt.y - 70);
    if (coord_reading_y > 180) {
        coord_reading_y = 180;
    }
    if (coord_reading_y < -180) {
        coord_reading_y = -180;
    }
    if (coord_reading_x > 240) {
        coord_reading_x = 240;
    }
    if (coord_reading_x < -240) {
        coord_reading_x = -240;
    }
    document.getElementById('coord_reading_x').textContent = coord_reading_x.toFixed(0);
    document.getElementById('coord_reading_y').textContent = coord_reading_y.toFixed(0);
    if (drag_selected !== null) {
        let offset_x = evt.x - drag_x;
        let offset_y = evt.y - drag_y;
        drag_selected.style.top = (parseInt(drag_selected.style.top, 10) + offset_y) + 'px';
        drag_selected.style.left = (parseInt(drag_selected.style.left, 10) + offset_x) + 'px';
    }
    drag_x = evt.x;
    drag_y = evt.y;
}, false);
const draggable_item = document.querySelector('.draggable');
draggable_item.addEventListener('mousedown', function(evt) {
    drag_selected = evt.target;
    drag_selected.style.filter = 'drop-shadow(5px 5px 1px lightgrey)';
}, false);
draggable_item.addEventListener('mouseup', function(evt) {
    drag_selected.style.filter = null;
    drag_selected = null;
}, false);
